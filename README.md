# Cursed Video Transform (Distorted Meme Generator)

Transforms a video file by distorting each frame (content-aware scaling or liquid-rescale, reducing the frame and keeping the pixels that seem "important") and distorting the audio.

This is a modified version of the original script "Distorted Meme Generator" by [zhavtom](https://github.com/zhavtom/distorted-meme-generator/commits?author=zhavtom), with flag parameters, improved error checking and a bunch of tweaks.

Requires bash, ffmpeg, imagemagick (with liquid rescale and the required libraries) python, and numpy.

## Important

Imagemagick requires [liquid rescale](https://imagemagick.org/script/command-line-options.php#liquid-rescale).

Recommended installation from [Imagemagick Unix Source](https://imagemagick.org/script/install-source.php) with the `libqlr` (or `libqlr-1-0` or `libqlr-1-0-dev`) installed beforehand.

If adding `--with-modules` to `./configure` isn't enough, try `--with-lqr`.

This program is not well optimized, there's a possibility that it'll slow your machine down while it extracts and converts the frames and audio.

## How to use

Run `./main_script.sh` in terminal with a given file input, wait a while, enjoy the result.

options:
- File input with `-f` followed by the "path/to/the/file.mp4". This is a *required* parameter.
- Seam carving amount (or scale) with `-s` followed by a number between 10-100. This is an *optional* parameter and the default value is 50.

Output file will be rendered at 25 fps as a `.webm` in the current directory as `cursed_originalfilename.webm`, with ogg vorbis encoding for the audio.

## Contact

Modified version: [Quentin Le Caignec](https://gitlab.com/QuentinLeCaignec)

Original Author: [zhavtom](https://github.com/zhavtom/distorted-meme-generator)
