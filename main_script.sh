#!/bin/bash

while getopts "f:s:" opt; do
    case $opt in
        f) file=$OPTARG  ;;
        s) scale=$OPTARG ;;
        *) echo "Error, option does not exist." >&2
        exit 1
    esac
done

# -f (input file) is *required*, valid file
if [ ! -f "$file" ]; then
    echo "Option -f (input file) missing or invalid path" >&2
    exit 1
fi

# -s (scale) is *required*, valid number
re='^[0-9][0-9]+$'
if ! [[ $scale =~ $re ]]; then
    if [[ $scale = "" ]]; then
        scale=50
    else
        echo "Option -s (liquid-rescale scale value) missing or invalid value" >&2
        exit 1
    fi
fi

# a couple variables
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
filename=${file%.*}

echo "--- Starting 'Distorted Meme Generator' script (liquid-rescale with ImageMagick) ---" >&2
echo "Input file: '$file'" >&2
echo "Liquid-rescale scale: $scale" >&2

echo -n "Making temporary folders for frame processing... " >&2
TMPDIR=$(mktemp -d)
echo "DONE" >&2

echo -n "Checking video format... " >&2
if [[ ${file##*.} =~ mov|mp4|avi|m4v|mkv|webm ]]; then
    echo "VALID" >&2
    
    echo "" >&2
    echo "Extracting frames... " >&2
    ffmpeg -y -i "${file}" -c:v mjpeg -r 25 $TMPDIR/frame_%03d.jpg
    echo "DONE" >&2
    
    echo -n "Distorting and resizing frames based on scale $scale... " >&2
    a=$scale
    b=$((100-$a))
    for f in $TMPDIR/frame_*; do
        fbname=${f##*/}
        f_scale=$((a+b))x$((a+b))%\!
        w=$(identify -format '%w' ${f})
        h=$(identify -format '%h' ${f})
        convert ${f} -liquid-rescale ${f_scale} $TMPDIR/lqr_${fbname}
        convert $TMPDIR/lqr_${fbname} -resize ${w}x${h} $TMPDIR/out_${fbname}
        deca=$(( $a * 99 ))
        a=$(( deca / 100 ))
    done
    echo "DONE" >&2
    
    echo -n "Checking for audio... " >&2
    has_audio=$(ffprobe -i "$file" -show_streams -select_streams a -loglevel error)
    if ! [[ $has_audio = "" ]]; then
        echo "FOUND" >&2
        echo "Extracting audio..." >&2
        ffmpeg -y -i "${file}" -vn $TMPDIR/extracted_audio.wav
        echo "DONE" >&2
        echo "Distorting audio... " >&2
        python -u $parent_path/distort_audio.py $TMPDIR $TMPDIR/extracted_audio.wav
        echo "DONE" >&2
    else
        echo "No audio found." >&2
    fi
    
    if [ -f "$TMPDIR/out_audio.wav" ]; then
        echo "Exporting distorted video + audio... " >&2
        ffmpeg -y -r 25 -i ${TMPDIR}/out_frame_%03d.jpg -i ${TMPDIR}/out_audio.wav -c:v libvpx-vp9 -crf 10 -c:a libvorbis -aq 4 cursed_${filename// /_}.webm
    else
        echo "Exporting distorted video... " >&2
        ffmpeg -y -r 25 -i ${TMPDIR}/out_frame_%03d.jpg -c:v libvpx-vp9 -crf 10 cursed_${filename// /_}.webm
    fi
    echo "DONE" >&2
else
    echo "Error" >&2
    exit 1
fi

sleep 1
echo "" >&2
echo -n "Cleaning up temporary folders... " >&2
rm -rf $TMPDIR
echo "DONE" >&2
